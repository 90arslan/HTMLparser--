================

--HTML-Parser--

================

===To Check the Accuracy of Interlinking Results of Cordis & OPenAire===

Input file: N triples
Output file: N triples


How to use:
-----------

```
git clone https://github.com/SANSA-Stack/SANSA-Template-Maven-Spark.git
cd SANSA-Template-Maven-Spark

mvn clean package
````

The subsequent steps depend on your IDE. Generally, just import this repository as a Maven project and start using SANSA / Spark. Enjoy it! :)

