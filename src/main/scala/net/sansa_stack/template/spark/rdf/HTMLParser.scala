package net.sansa_stack.template.spark.rdf

import java.io._
import java.net.{URI, URL, HttpURLConnection}
import scala.io.Source
import net.sansa_stack.rdf.spark.io.NTripleReader
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.jena.graph.Triple
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._

object HTMLParser extends Serializable {
    def main(args: Array[String]): Unit = {
        println("=========================")
        println("|      HTML Parser      |")
        println("=========================")

        val input = "/home/arslan/Documents/HTML-Parsing/src/main/resources/input/input.nt"
        val output = "/home/arslan/Documents/HTML-Parsing/src/main/resources/output"

        val spark = SparkSession.builder
            .appName("HTML Parser")
            .master("local[*]")
            .getOrCreate()

        val nTriplesRDD = NTripleReader.load(spark, URI.create(input))

        // perform html parsing and find common triples of both data-sets.
        this.runHtmlParser(nTriplesRDD, output)

        // stop spark
        spark.stop
    }

    /**
      * perform html parsing and find common triples of both data-sets.
      *
      * @param nTriplesRDD
      * @param output
      */
    def runHtmlParser(nTriplesRDD: RDD[Triple], output: String): Unit = {
        nTriplesRDD.filter(line => {
            val browser = JsoupBrowser()
            var openaireTitle: String = ""
            var cordisTitle: String = ""

            // openaire
            val openaire = line.getSubject.toString()
            try {
                val test = get(openaire)
                val document_openaire = browser.get(openaire)
                if (document_openaire.toHtml.nonEmpty) {
                    val openaire_items = document_openaire >> elementList("span.literal")
                    if (openaire_items.nonEmpty) {
                        openaire_items.foreach(item => {
                            val innerHtml = item.innerHtml
                            if (innerHtml.contains("projectTitle") || innerHtml.contains("prefLabel")) {
                                openaireTitle = (item >> allText).toLowerCase
                            }
                        })
                    }
                }
            } catch {
                case ioe: java.io.IOException => openaireTitle = ""
                case ste: java.net.SocketTimeoutException => openaireTitle = ""
            }

            // cordis
            val cordis = line.getObject.toString()
            try {
                val test = get(cordis)
                val document_cordis = browser.get(cordis)
                if (document_cordis.toHtml.nonEmpty) {
                    val cordis_items = document_cordis >> elementList("img.qrcode + h2")
                    if (cordis_items.nonEmpty) {
                        cordis_items.foreach(item => {
                            cordisTitle = (item >> text).toLowerCase
                        })
                    }
                }
            } catch {
                case ioe: java.io.IOException => cordisTitle = ""
                case ste: java.net.SocketTimeoutException => cordisTitle = ""
            }

            openaireTitle.nonEmpty && cordisTitle.nonEmpty && openaireTitle == cordisTitle
        }).repartition(1).saveAsTextFile(output)
    }

    /**
      * return url content if url is alive otherwise return an error
      *
      * @param url
      * @param connectTimeout
      * @param readTimeout
      * @param requestMethod
      * @return
      */
    def get(url: String, connectTimeout: Int = 5000, readTimeout: Int = 5000, requestMethod: String = "GET") = {
        val connection = new URL(url).openConnection.asInstanceOf[HttpURLConnection]
        connection.setConnectTimeout(connectTimeout)
        connection.setReadTimeout(readTimeout)
        connection.setRequestMethod(requestMethod)

        val inputStream = connection.getInputStream
        val content = Source.fromInputStream(inputStream).mkString

        if (inputStream != null) {
            inputStream.close
        }

        content
    }
}
